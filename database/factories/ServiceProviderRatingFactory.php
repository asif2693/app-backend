<?php

use Faker\Generator as Faker;

$factory->define(App\ServiceProviderRating::class, function (Faker $faker) {
    return [
        'service_provider_id' => \App\ServiceProvider::inRandomOrder()->value('id'),
        'satisfaction_rating' => array_random([1,2,3,4,5]),
        'service_on_time_rating' => array_random([1,2,3,4,5]),
        'service_on_budget_rating' => array_random([1,2,3,4,5]),
        'feedback' => $faker->sentence,
        'rated_by' => App\Consumer::inRandomOrder()->first()->user->id
    ];
});
