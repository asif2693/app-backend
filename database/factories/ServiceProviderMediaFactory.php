<?php

use Faker\Generator as Faker;

$factory->define(App\ServiceProviderMedia::class, function (Faker $faker) {
    return [
        'asset_url' => $faker->imageUrl(),
        'service_provider_id' => \App\ServiceProvider::inRandomOrder()->first()->id
    ];
});
