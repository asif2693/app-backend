<?php

use Faker\Generator as Faker;

$factory->define(App\SubCategory::class, function (Faker $faker) {
    return [
        'name_en' => str_random(8),
        'name_fr' => str_random(8),
        'name_ar' => str_random(8),
        'name_ur' => str_random(8),
        'asset_url_en' => $faker->imageUrl(20, 20),
        'asset_url_fr' => $faker->imageUrl(20, 20),
        'asset_url_ar' => $faker->imageUrl(20, 20),
        'asset_url_ur' => $faker->imageUrl(20, 20),
    ];
});
