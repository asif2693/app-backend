<?php

use Faker\Generator as Faker;

$factory->define(App\ServiceProvider::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'user_id' => factory(App\User::class)->create()->id,
        'description' => $faker->sentence,
        'address' => $faker->address,
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
        'contact_details' => [
            'phone_numbers' => $faker->phoneNumber
        ],
        'cost_slab_first' => array_random(range(13,18)),
        'cost_slab_second' => array_random(range(9,12)),
        'cost_slab_third' => array_random(range(5,9)),
        'featured_image' => $faker->imageUrl(),
        'open_time' => array_random(['00:00', '1:00', '2:00', '3:00', '4:00', '5:00', '6:00', '7:00', '8:00']),
        'close_time' => array_random(['12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00']),
    ];
});

$factory->afterCreating(App\ServiceProvider::class, function ($service_provider, $faker) {
    \App\SubCategory::inRandomOrder()->limit(10)->get()->map(function($sub_category) use ($service_provider) {
        \Illuminate\Support\Facades\DB::insert("Insert into service_provider_sub_category (sub_category_id, service_provider_id) values ({$sub_category->id}, {$service_provider->id})");
    });
});

$factory->afterCreating(App\ServiceProvider::class, function ($service_provider, $faker) {
    \App\Category::inRandomOrder()->limit(3)->get()->map(function($category) use ($service_provider) {
        \Illuminate\Support\Facades\DB::insert("Insert into category_service_provider (category_id, service_provider_id) values ({$category->id}, {$service_provider->id})");
    });
});
