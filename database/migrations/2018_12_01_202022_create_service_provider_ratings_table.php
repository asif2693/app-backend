<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceProviderRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_provider_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('service_provider_id')->index();
            $table->string('rating');
            $table->unsignedInteger('rated_by')->index();
            $table->string('feedback')->index()->nullable();
            $table->timestamps();
        });
        Schema::table('service_provider_ratings', function(Blueprint $table) {
            $table->foreign('service_provider_id')
                ->references('id')
                ->on('service_providers')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_provider_ratings');
    }
}
