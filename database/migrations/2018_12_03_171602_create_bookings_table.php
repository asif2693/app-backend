<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('consumer_id')->index();
            $table->unsignedInteger('service_provider_id')->nullable()->index();
            $table->string('latitude');
            $table->string('longitude');
            $table->timestamp('date_time');
            $table->string('title');
            $table->string('description')->nullable();
            $table->longText('media')->nullable();
            $table->tinyInteger('job_type')->default(1)->comment('1 for fixed, 2 for hourly');
            $table->string('job_period')->nullable();
            $table->tinyInteger('status')->default(0)->comment('0 => pending | 1 => accepted | 2 => on-hold | 3 => declined | 4 => canceled | 5 => completed');
            $table->string('final_price')->nullable();
            $table->boolean('is_negotiable')->default(false);
            $table->unsignedInteger('category_id')->index();
            $table->unsignedInteger('sub_category_id')->index();
            $table->timestamps();
        });

        Schema::table('bookings', function(Blueprint $table) {
            $table->foreign('service_provider_id')
                ->references('id')
                ->on('service_providers')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

            $table->foreign('consumer_id')
            ->references('id')
            ->on('consumers')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');


            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

            $table->foreign('sub_category_id')
                ->references('id')
                ->on('sub_categories')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
