<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInServiceProvidersRatingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_provider_ratings', function (Blueprint $table) {
            $table->dropColumn('rating');
            $table->integer('satisfaction_rating')->default(0);
            $table->integer('service_on_time_rating')->default(0);
            $table->integer('service_on_budget_rating')->default(0);
            $table->unsignedInteger('rated_by')->index()->nullable();
        });
        Schema::table('service_provider_ratings', function (Blueprint $table) {
            $table->foreign('rated_by')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_provider_ratings', function (Blueprint $table) {
            //
        });
    }
}
