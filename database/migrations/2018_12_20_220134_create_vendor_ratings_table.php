<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('satisfaction_rating');
            $table->integer('service_on_time_rating');
            $table->integer('service_on_budget_rating');
            $table->string('feedback');
            $table->unsignedInteger('vendor_id')->index();
            $table->unsignedInteger('rated_by')->index();
            $table->timestamps();
        });

        Schema::table('vendor_ratings', function (Blueprint $table) {
            $table->foreign('vendor_id')
                ->references('id')
                ->on('vendors')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('rated_by')
                ->references('id')
                ->on('consumers')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_ratings');
    }
}
