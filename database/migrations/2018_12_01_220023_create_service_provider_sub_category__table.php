<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceProviderSubCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_provider_sub_category', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sub_category_id')->index();
            $table->unsignedInteger('service_provider_id')->index();
            $table->timestamps();
        });
        Schema::table('service_provider_sub_category', function(Blueprint $table) {
            $table->foreign('service_provider_id')
                ->references('id')
                ->on('service_providers')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('sub_category_id')
                ->references('id')
                ->on('sub_categories')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_provider_sub_category');
    }
}
