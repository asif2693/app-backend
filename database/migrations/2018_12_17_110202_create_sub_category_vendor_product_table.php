<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubCategoryVendorProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_category_vendor_product', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sub_category_id')->index();
            $table->unsignedInteger('vendor_product_id')->index();
            $table->timestamps();
        });

        Schema::table('sub_category_vendor_product', function (Blueprint $table) {
            $table->foreign('sub_category_id')
                ->references('id')
                ->on('sub_categories')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('vendor_product_id')
                ->references('id')
                ->on('vendor_products')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_category_vendor_product');
    }
}
