<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Category::class, 5)->create();
        factory(App\User::class, 100)->create();
        factory(App\ServiceProvider::class, 50)->create();
        factory(\App\ServiceProviderRating::class, 200)->create();
        factory(\App\ServiceProviderMedia::class, 500)->create();
        factory(\App\VendorProduct::class, 1000)->create();
    }
}
