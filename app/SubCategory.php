<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function service_providers()
    {
        return $this->belongsToMany(ServiceProvider::class);
    }

    public function vendors()
    {
        return $this->belongsToMany(Vendor::class);
    }

    public function vendor_products()
    {
        return $this->belongsToMany(VendorProduct::class);
    }
}
