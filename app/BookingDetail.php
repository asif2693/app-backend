<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingDetail extends Model
{
    Protected $guarded = [];


    public function product()
    {
        return $this->belongsTo(VendorProduct::class);
    }
}
