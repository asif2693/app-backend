<?php

if(! function_exists('get_min_max_lat_long')) {
    function get_min_max_lat_long($latitude, $longitude)
    {
        $distance = 5; //km
        $radius = 6371; //This is the radius of earth in KMs
        return [
            'max_lat' => $latitude + rad2deg($distance / $radius),
            'min_lat' => $latitude - rad2deg($distance / $radius),
            'max_long' => $longitude + rad2deg($distance / $radius / cos(deg2rad($latitude))),
            'min_long' => $longitude - rad2deg($distance / $radius / cos(deg2rad($latitude))),
        ];
    }
}