<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consumer extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class, 'consumer_id');
    }

    public function ratings()
    {
        return $this->hasMany(ConsumerRating::class, 'consumer_id');
    }
}
