<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $guarded = [];

    protected $appends = [
        'avg_rating'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function sub_categories()
    {
        return $this->belongsToMany(SubCategory::class);
    }

    public function vendor_products()
    {
        return $this->hasMany(VendorProduct::class);
    }

    public function rating()
    {
        return $this->hasMany(VendorRating::class);
    }

    public function getAvgRatingAttribute()
    {
        return $this->rating()->count() === 0  ? 0 : $this->rating()->sum('satisfaction_rating') / $this->rating()->count();
    }
}
