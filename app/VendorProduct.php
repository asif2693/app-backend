<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorProduct extends Model
{


    protected $casts = [
        'image' => 'array',
    ];


    protected $guarded = [];

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function sub_categories()
    {
        return $this->belongsToMany(SubCategory::class);
    }
}
