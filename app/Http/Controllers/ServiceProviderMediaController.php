<?php

namespace App\Http\Controllers;

use App\ServiceProviderMedia;
use Illuminate\Http\Request;

class ServiceProviderMediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceProviderMedia  $serviceProviderMedia
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceProviderMedia $serviceProviderMedia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceProviderMedia  $serviceProviderMedia
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceProviderMedia $serviceProviderMedia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceProviderMedia  $serviceProviderMedia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceProviderMedia $serviceProviderMedia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceProviderMedia  $serviceProviderMedia
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceProviderMedia $serviceProviderMedia)
    {
        //
    }
}
