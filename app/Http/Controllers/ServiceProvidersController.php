<?php

namespace App\Http\Controllers;

use App\Category;
use App\ServiceProvider;
use App\ServiceProviderDetail;
use App\SubCategory;
use Illuminate\Http\Request;

class ServiceProvidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'success' => true,
            'service_providers' => ServiceProvider::with('media', 'rating')->paginate(10)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255|unique:service_providers',
            'description' => 'required|min:10|max:255',
            'open_time' => 'required',
            'close_time' => 'required',
            'address' => 'required|mix:255',
            'latitude' => 'required',
            'longitude' => 'required',
            'contact_details' => 'required',
            'featured_image' => 'required',
        ]);

        $service_provider = ServiceProvider::create(request()->all());

        return response()->json([
            'success' => true,
            'service_provider' => $service_provider,
        ], 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceProviderDetail  $serviceProviderDetail
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceProviderDetail $serviceProviderDetail)
    {
        return response()->json([
            'success' => true,
            'service_provider' => $serviceProviderDetail->load('avg_rating', 'media')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceProviderDetail  $serviceProviderDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceProviderDetail $serviceProviderDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceProviderDetail  $serviceProviderDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceProviderDetail $serviceProviderDetail)
    {
        //
    }

    public function getByFilters()
    {
        $this->validate(request(), [
            'category_id' => ['required_without:sub_category_id', 'exists:categories'],
            'sub_category_id' => ['required_without:sub_category_id', 'exists:sub_categories'],
            'latitude' => ['required'],
            'longitude' => ['required']
        ]);
        $list = collect();
        $serviceProvidersWithCategory = Category::where('id', request('category_id'))->with('service_providers_by_location')->first();
        if(!empty($serviceProvidersWithCategory)) {
            $list->push($serviceProvidersWithCategory->service_providers_by_location);
        }
        $serviceProvidersWithSubCategory = SubCategory::where('id', request('sub_category_id'))->with('service_providers_by_location')->first();
        if(!empty($serviceProvidersWithSubCategory)) {
            if($list->isEmpty()) {
                $list = $serviceProvidersWithSubCategory->service_providers_by_location;
                return response()->json([
                    'success' => true,
                    'service_providers' => $list
                ]);
            }
            $list->merge($serviceProvidersWithSubCategory->service_providers_by_location);
            return response()->json([
                'success' => true,
                'service_provider' => $list
            ]);
        }
        return $list->isEmpty()
            ? response()->json(['success' => false, 'message' => 'No messages found in your locality'], 404)
            : response()->json(['success' => true, 'service_providers' => $list]);
    }
}
