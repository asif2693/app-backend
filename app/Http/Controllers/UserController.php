<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function update()
    {
        $this->validate(request(), [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'number' => ['required', 'string', 'min:6', 'max:20'],
            'image' => ['sometimes', 'image'],
        ]);

        if(request()->has('image')) {

        }
        request()->user()->update([

        ]);
        return response()->json([
            'success' => true,
            'data' =>
        ]);
    }
}
