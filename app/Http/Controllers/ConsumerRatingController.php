<?php

namespace App\Http\Controllers;

use App\ConsumerRating;
use Illuminate\Http\Request;

class ConsumerRatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->valiedate(request(), [
            'rating' => 'required|max:2',
            'feedback' => 'sometimes|max:255',
            'booking_id' => 'required|exists:bookings',
            'consumer_id' => 'required|exists:consumers',
        ]);

        $rating = ConsumerRating::create(request()->all());
        return response()->json([
            'success' => true,
            'rating' => $rating
        ], 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ConsumerRating  $consumerRating
     * @return \Illuminate\Http\Response
     */
    public function show(ConsumerRating $consumerRating)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ConsumerRating  $consumerRating
     * @return \Illuminate\Http\Response
     */
    public function edit(ConsumerRating $consumerRating)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ConsumerRating  $consumerRating
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ConsumerRating $consumerRating)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ConsumerRating  $consumerRating
     * @return \Illuminate\Http\Response
     */
    public function destroy(ConsumerRating $consumerRating)
    {
        //
    }
}
