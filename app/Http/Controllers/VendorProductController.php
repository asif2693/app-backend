<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VendorProduct;

class VendorProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
    	$data = VendorProduct::where('vendor_id' , $id)->get();

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }


}
