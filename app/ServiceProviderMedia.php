<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceProviderMedia extends Model
{
    protected $table = 'service_provider_media';

    protected $guarded = [];

    public function service_provider()
    {
        return $this->belongsTo(ServiceProvider::class, 'service_provider_id');
    }
}
