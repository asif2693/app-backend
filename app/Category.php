<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function sub_categories()
    {
        return $this->hasMany(SubCategory::class);
    }

    public function service_providers()
    {
        return $this->belongsToMany(ServiceProvider::class);
    }

    public function vendor_products()
    {
        return $this->belongsToMany(VendorProduct::class);
    }

    public function vendors()
    {
        return $this->belongsToMany(Vendor::class);
    }

    public function service_providers_by_location()
    {
        $latLong = get_min_max_lat_long(request('latitude'), request('longitude'));
        return $this->belongsToMany(ServiceProvider::class, 'category_service_provider_detail', 'service_provider_detail_id', 'category_id')
            ->withTimestamps()->whereBetween('latitude', array($latLong['min_lat'], $latLong['max_lat']))
            ->whereBetween('longitude', array($latLong['min_long'], $latLong['max_long']))->with('avg_rating', 'media');
    }
}
