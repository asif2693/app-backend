<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceProviderDetail extends Model
{
    protected $table = 'service_provider_details';

    protected $guarded = [];

    public function media()
    {
        return $this->hasMany(ServiceProviderMedia::class, 'service_provider_details_id');
    }

    public function rating()
    {
        return $this->hasMany(ServiceProviderRating::class, 'service_provider_details_id');
    }

    public function avg_rating()
    {
        return $this->rating()->sum('rating') / $this->rating()->count();
    }

}
