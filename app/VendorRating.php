<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorRating extends Model
{
    protected $guarded = [];

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function rated_by()
    {
        return $this->belongsTo(Consumer::class, 'rated_by');
    }
}
