    // Route components
    import VueRouter from 'vue-router'

    const routes = [

    /* Dashboard */

/*    {
        name: 'dashboard',
        path: '/dashboard',
        meta: {
            title: 'Dashboard',
            bodyClass: 'dashboard-page',
            pagetitle:'Dashboard',
        },
        component: require('./components/Dashboard.vue'),
    },*/

    /*  Page One  */
    {
        path: '/components',
        meta: {
            title: 'Page-one',
            bodyClass: 'components-body',
            pagetitle:'Components',
            requiresAuth: true,
        },
        component: require('./components/Components.vue'),
    },
    {
        path: '/',
        meta: {
            title: 'Page-one',
            bodyClass: 'components-body',
            pagetitle:'Components',
        },
        component: require('./components/admin/auth/Login.vue'),
    },
    {
        name: 'login',
        path: '/login',
        meta: {
            title: 'Login',
            bodyClass: 'components-body',
            pagetitle:'Components',
        },
        component: require('./components/admin/auth/Login.vue'),
    },
    /* Configurations */
    {
        path: '/admin',
        meta: {
            title: 'Configurations',
            bodyClass: 'configurations-body',
            requiresAuth: true,
        },
        component: require('./components/admin/super_admin/Main.vue'),
        children: [
        {
            name: 'admin.dashboard',
            path: 'dashboard',
            meta: {
                title: 'Dashboard',
                bodyClass: 'components-body',
                pagetitle:'Components',
                requiresAuth : true
            },
            component: require('./components/admin/super_admin/Dashboard.vue'),
        },
        {
            path: 'category',
            meta: {
                title: 'Category',
                bodyClass: 'components-body',
                pagetitle:'Components',
                requiresAuth : true
            },
            component: require('./components/admin/category/Main.vue'),
            children: [
            {
                name: 'admin.category.add',
                path: 'add',
                meta: {
                    title: 'Dashboard',
                    bodyClass: 'components-body',
                    pagetitle:'Components',
                    requiresAuth : true
                },
                component: require('./components/admin/category/Add.vue'),
            },
            {
                name: 'admin.category.list',
                path: 'list',
                meta: {
                    title: 'Dashboard',
                    bodyClass: 'components-body',
                    pagetitle:'Components',
                    requiresAuth : true
                },
                component: require('./components/admin/category/List.vue'),
            }

            ]
        },
        {
            path: 'sub-category',
            meta: {
                title: 'Sub Category',
                bodyClass: 'components-body',
                pagetitle:'Components',
                requiresAuth : true
            },
            component: require('./components/admin/sub_category/Main.vue'),
            children: [
            {
                name: 'admin.sub.category.add',
                path: 'add',
                meta: {
                    title: 'Dashboard',
                    bodyClass: 'components-body',
                    pagetitle:'Components',
                    requiresAuth : true
                },
                component: require('./components/admin/sub_category/Add.vue'),
            },
            {
                name: 'admin.sub.category.list',
                path: 'list',
                meta: {
                    title: 'Dashboard',
                    bodyClass: 'components-body',
                    pagetitle:'Components',
                    requiresAuth : true
                },
                component: require('./components/admin/sub_category/List.vue'),
            }

            ]
        },
        {
            path: 'vendor',
            meta: {
                title: 'Sub Category',
                bodyClass: 'components-body',
                pagetitle:'Components',
                requiresAuth : true
            },
            component: require('./components/admin/vendor/Main.vue'),
            children: [
            {
                name: 'admin.vendor.add',
                path: 'add',
                meta: {
                    title: 'Dashboard',
                    bodyClass: 'components-body',
                    pagetitle:'Components',
                    requiresAuth : true
                },
                component: require('./components/admin/vendor/Add.vue'),
            },
            {
                name: 'admin.vendor.list',
                path: 'list',
                meta: {
                    title: 'Dashboard',
                    bodyClass: 'components-body',
                    pagetitle:'Components',
                    requiresAuth : true
                },
                component: require('./components/admin/vendor/List.vue'),
            },
            {
                name: 'admin.vendor.items',
                path: 'items',
                meta: {
                    title: 'Dashboard',
                    bodyClass: 'components-body',
                    pagetitle:'Components',
                    requiresAuth : true
                },
                component: require('./components/admin/vendor/Items.vue'),
            },
            {
                name: 'admin.vendor.ratings',
                path: 'ratings',
                meta: {
                    title: 'Dashboard',
                    bodyClass: 'components-body',
                    pagetitle:'Components',
                    requiresAuth : true
                },
                component: require('./components/admin/vendor/Ratings.vue'),
            },
            {
                name: 'admin.vendor.sales',
                path: 'sales',
                meta: {
                    title: 'Dashboard',
                    bodyClass: 'components-body',
                    pagetitle:'Components',
                    requiresAuth : true
                },
                component: require('./components/admin/vendor/Sales.vue'),
            }




            ]
        },
        {


            path: 'service-provider',
            meta: {
                title: 'Sub Category',
                bodyClass: 'components-body',
                pagetitle:'Components',
                requiresAuth : true
            },
            component: require('./components/admin/service_provider/Main.vue'),
            children: [
            {
                name: 'admin.service.provider.add',
                path: 'add',
                meta: {
                    title: 'Dashboard',
                    bodyClass: 'components-body',
                    pagetitle:'Components',
                    requiresAuth : true
                },
                component: require('./components/admin/service_provider/Add.vue'),
            },
            {
                name: 'admin.service.provider.list',
                path: 'list',
                meta: {
                    title: 'Dashboard',
                    bodyClass: 'components-body',
                    pagetitle:'Components',
                    requiresAuth : true
                },
                component: require('./components/admin/service_provider/List.vue'),
            },
            {
                name: 'admin.service.provider.items',
                path: 'items',
                meta: {
                    title: 'Dashboard',
                    bodyClass: 'components-body',
                    pagetitle:'Components',
                    requiresAuth : true
                },
                component: require('./components/admin/service_provider/Items.vue'),
            },
            {
                name: 'admin.service.provider.ratings',
                path: 'ratings',
                meta: {
                    title: 'Dashboard',
                    bodyClass: 'components-body',
                    pagetitle:'Components',
                    requiresAuth : true
                },
                component: require('./components/admin/service_provider/Ratings.vue'),
            },
            {
                name: 'admin.service.provider.sales',
                path: 'sales',
                meta: {
                    title: 'Dashboard',
                    bodyClass: 'components-body',
                    pagetitle:'Components',
                    requiresAuth : true
                },
                component: require('./components/admin/service_provider/Sales.vue'),
            }


            ]


        }

        ]
    }


    /*404 Page*/
/*
    {
        name: '404',
        path: '*',
        component: require('./components/404/Main.vue'),
        meta: {
            title: '404 Not Found',
            noHeader: true,
            bodyClass: 'not-found-page',
        },
    },*/

    ]



// Create the router instance
const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
})

router.beforeEach((to, from, next) => {


    if (to.matched.some(record => record.meta.requiresAuth) && !router.app.$auth.isAuthenticated()) {
        next({ name: 'login' });
    } else if (!to.matched.some(record => record.meta.requiresAuth) && router.app.$auth.isAuthenticated()) {
        next({ name: 'admin.dashboard' });

    } else {
        next();
    }
})



export default router
