/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');
 window.Vue = require('vue');

 import Vue from 'vue';
 import VueRouter from 'vue-router';
 import router from './routes';
 import ShardsVue from 'shards-vue'
 import AmCharts from 'amcharts3';
 import AmSerial from 'amcharts3/amcharts/serial';
 import AmPie from 'amcharts3/amcharts/pie';
 import AmFunnelChart  from 'amcharts3/amcharts/funnel';
 import AmGauge from 'amcharts3/amcharts/gauge';
 import vbclass from 'vue-body-class';
 var VueScrollTo = require('vue-scrollto');
 import VueTimepicker from 'vue2-timepicker';
 import Selectize from 'vue2-selectize';
 import VueCodeHighlight from 'vue-code-highlight';
 var vueDatepicker = require("vue-datepicker");
 import Paginate from 'vuejs-paginate';
 Vue.component('paginate', Paginate);
 import StarRating from 'vue-star-rating';
 import Toasted from 'vue-toasted';
 import VeeValidate from 'vee-validate'
 import VueAxios from 'vue-axios';
 import VueAuthenticate from 'vue-authenticate';
 import axios from 'axios';
 import Vuex from 'vuex';
 import store from './store.js';


 let veeCustomMessage = {
    en: {
        custom: {
            agree: {
                required: 'You must agree to the terms and conditions before registering!',
                digits: (field, params) => `length must be ${params[0]}`
            },
            privacypolicy: {
                required: 'You must agree the privacy policy before registering!',
                digits: (field, params) => `length must be ${params[0]}`
            },
            password_confirmation: {
                confirmed: 'Password does not match.'
            }
        }
    }
};
const config = {
    errorBagName: 'errorBag', // change if property conflicts.
    dictionary:  veeCustomMessage,
    events: 'input' 
};


Vue.use(VueAxios, axios)

Vue.use(VueAuthenticate, {
    tokenName: 'admin_access_token',
    baseUrl: '/',
    loginUrl: '/api/auth/login',
    registerUrl: '/api/auth/register',
    logoutUrl: '/api/auth/logout',
    storageType: 'cookieStorage',
    providers: {
        // Define OAuth providers config
        oauth2: {
            name: 'oauth2',
            url: 'Token/Exchange',
        }
    }
})

Vue.use(Vuex);
Vue.use(VeeValidate,config);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
 Vue.use(VueRouter);
 Vue.use(router);
 Vue.use(ShardsVue);
 Vue.use( vbclass, router );
 Vue.use(VueTimepicker);
 Vue.use(Selectize);
 Vue.use(VueCodeHighlight);
 Vue.use(StarRating);
 Vue.use(Toasted);
 Vue.use(VueScrollTo, {
    container: "body",
    duration: 2000,
    easing: "ease",
    offset:-65,
    cancelable: true,
    onStart: false,
    onDone: false,
    onCancel: false,
    x: false,
    y: true
});







// Require components tags
require('./components-tags');
require('./filters');





const app = new Vue({
    el: '#app',
    store,

    router, // short for `routes: routes`

});


